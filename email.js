const nodemailer = require('nodemailer')
//
// // async..await is not allowed in global scope, must use a wrapper
export async function sendEmail({
  from,
  to,
  subject,
  text,
  html,
  replyTo
}) {
  const transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: process.env.SMTP_PORT,
    secure: false, // true for 465, false for other ports
    auth: {
      user: process.env.SMTP_USER, // generated ethereal user
      pass: process.env.SMTP_PASS // generated ethereal password
    }
  })
  const mail = {
    from,
    to,
    subject,
    text,
    html,
    replyTo
  }
  const info = await transporter.sendMail(mail)

  return info
}
