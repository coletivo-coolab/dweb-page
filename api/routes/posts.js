const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const slugify = require('slugify')
const auth = require('../config/auth')
const Post = mongoose.model('Post')

router.get('/', async (req, res) => {
  const query = {}

  if (req.query.search) {
    query.title = { $regex: req.query.search, $options: 'i' }
  }

  if (req.query.tag) {
    query.tags = req.query.tag
  }
  try {
    const posts = await Post.find(query)
      .populate(req.query.populate)
      .sort({ publishing_date: -1 })
    res.json(posts)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/current_tags', async (req, res) => {
  try {
    const posts = await Post.find().select('tags')
    const tags = {}
    posts.forEach(post => {
      if (post.tags) {
        post.tags.forEach(tag => {
          tags[tag] = true
        })
      }
    })
    res.json(Object.keys(tags).sort((a, b) => a.localeCompare(b)))
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/:id', async (req, res) => {
  try {
    const post = await Post.findOne({
      slug: req.params.id
    })
    res.json(post)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.post('/', auth.admin, async (req, res) => {
  const newPost = new Post(req.body)
  if (!newPost.slug) {
    newPost.slug = slugify(newPost.title).toLowerCase()
  }

  try {
    await newPost.save()
    res.send(newPost)
  } catch (error) {
    res.status(422).send(error.message)
  }
})

router.put('/:id', auth.admin, async (req, res) => {
  const params = req.body
  const post = await Post.findOne({ _id: req.params.id })
  Object.keys(params).forEach(key => {
    post[key] = params[key]
  })
  await post
    .save()
    .then(async post => {
      res.send(await Post.findById(post._id))
    })
    .catch(err => {
      res.status(422).send(err.message)
    })
})

router.delete('/:id', auth.admin, async (req, res) => {
  try {
    const post = await Post.findByIdAndDelete(req.params.id)

    res.send(post)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
