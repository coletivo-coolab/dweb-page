const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const auth = require('../config/auth')
const Contact = mongoose.model('Contact')
const Settings = mongoose.model('Settings')
const { sendEmail } = require('../../email')

router.get('/', auth.admin, async (req, res) => {
  try {
    const contacts = await Contact.find().sort({ createdAt: -1 })
    res.send(contacts)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/unread', auth.admin, async (req, res) => {
  const query = {
    read: { $ne: true }
  }

  try {
    const contactsQtd = await Contact.count(query)
    res.json(contactsQtd)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/:id', auth.admin, async (req, res) => {
  const contact = await Contact.findById(req.params.id)
  if (!contact.read) {
    contact.read = true
    await contact.save()
  }
  res.json(contact)
})

router.post('/contact', (req, res) => {
  const contact = new Contact()

  contact.name = req.body.name
  contact.email = req.body.email
  contact.message = req.body.message

  contact.save().then(() => {
    sendEmailMessage(contact)
    res.json(contact)
  }).catch(err => {
    res.status(422).send(err.message)
  })
})

router.delete('/:id', auth.admin, async (req, res) => {
  try {
    const contact = await Contact.findByIdAndDelete(req.params.id)
    res.send(contact)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

const sendEmailMessage = async (contact) => {
  try {
    const settings = await Settings.findOne()
    const email = {
      from: '"' + settings.title + '" <' + settings.email + '>',
      to: settings.email,
      replyTo: contact.email,
      subject: 'Nova mensagem de contato no site de ' + settings.email,
      text: `
      Nome: ${contact.name}
      Email: ${contact.email}
      Mensagem: ${contact.message}
    `
    }
    sendEmail(email)
  } catch (error) {
    console.log(error)
  }
}
router.post('/delete', auth.admin, (req, res) => {
  req.body.forEach(async item => {
    await Contact.findByIdAndDelete(item._id)
  })
  res.send(true)
})

router.post('/read', auth.admin, (req, res) => {
  try {
    req.body.forEach(async item => {
      await Contact.findOneAndUpdate({
        _id: item._id
      }, {
        $set: { read: 'true' }
      }, {
        upsert: true
      })
    })
    res.send(true)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
