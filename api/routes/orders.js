const express = require('express')
const mongoose = require('mongoose')
const router = express.Router()
const { authenticated, admin } = require('../config/auth')
const Order = mongoose.model('Order')

router.get('/', authenticated, async (req, res) => {
  const query = { }

  if (req.user.role === 'user') {
    query.user = req.user._id
  }

  try {
    const orders = await Order.find(query)
      .populate({
        path: 'items.product',
        model: 'Product'
      })
      .sort({
        code: -1
      })

    res.send(orders)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.get('/:id', authenticated, async (req, res) => {
  const query = {
    _id: req.params.id
  }

  if (req.user.role === 'user') {
    query.user = req.user._id
  }

  try {
    const order = await Order.findOne(query)
      .populate([
        'user',
        {
          path: 'items.product',
          model: 'Product'
        }
      ])
    res.send(order)
  } catch (err) {
    res.status(422).send(err.message)
  }
})

router.put('/:id', admin, async (req, res) => {
  const query = {
    _id: req.params.id
  }

  try {
    const order = await Order.findOne(query)
    const status = req.body.status
    if (status) {
      order.status = status
    }
    order.save().then(function() {
      res.json(order)
    })
  } catch (err) {
    res.status(422).send(err.message)
  }
})

module.exports = router
