require('dotenv').config()

require('./models/Settings')
require('./models/Attachment')
require('./models/User')
require('./models/Menu')
require('./models/Post')
require('./models/Event')
require('./models/Page')
require('./models/Contact')
require('./models/Media')
require('./models/Product')
require('./models/Order')
require('./config/passport')

const mongoose = require('mongoose')
const isProduction = process.env.NODE_ENV === 'production'

const appName = process.env.APP_NAME || 'cms'

let databaseUri = 'mongodb://localhost/' + appName

if (process.env.MONGO_URI) {
  if (process.env.MONGO_URI.includes('mongodb+srv')) {
    databaseUri = process.env.MONGO_URI
  } else {
    databaseUri =
      'mongodb://' + (process.env.MONGO_URI || 'localhost') + '/' + appName
  }
}

if (isProduction) {
  mongoose.set('debug', false)
  mongoose.connect(databaseUri, {
    keepAlive: true,
    connectTimeoutMS: 30000,
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
} else {
  mongoose.set('debug', true)
  mongoose.connect(databaseUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
}
