const mongoose = require('mongoose')

const AttachmentSchema = new mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  url: {
    type: String,
    required: true
  },
  thumb: {
    type: String
  },
  average: {
    type: String
  },
  title: {
    type: String
  },
  description: {
    type: String
  },
  credits: {
    type: String
  },
  link: {
    type: String
  },
  link_title: {
    type: String
  },
  counter: { // Contador de downloads dos arquivos
    type: Number,
    default: 0
  }
})

const Attachment = mongoose.models.Attachment || mongoose.model('Attachment', AttachmentSchema)
module.exports = Attachment
